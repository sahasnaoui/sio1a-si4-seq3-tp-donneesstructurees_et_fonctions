
package exercices;
import donnees.Club;
import donnees.Personne;


public class Question04 {

    public static void main(String[] args) {
         System.out.println("Effectif du club :\n");
         int nbHommes=0, nbFemmes=0;
         for (Personne pers : Club.listeDesPersonnes){
             if (pers.sexe.equals("M")){
                nbHommes++;
             if (pers.sexe.equals("F")){
                 nbFemmes++;
             }
             }
             
             
             System.out.println("Nombres d'Hommes : " +nbHommes);
             System.out.println("Nombres de femmes : " +nbFemmes);
             System.out.println("Nombres Total : " + (nbHommes+nbFemmes));
         } 
       
    }
}

