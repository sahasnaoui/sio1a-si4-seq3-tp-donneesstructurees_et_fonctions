
package exercices;
import donnees.Club;
import donnees.Personne;

public class Question06 {

    public static void main(String[] args) {
        float min=200.0f;
        for (Personne pers : Club.listeDesPersonnes){
            if(pers.sexe.equals("M") && pers.poids<min){
                min=pers.poids;
            }
        }
          System.out.println("Poids le plus bas cher les judokas Hommes : "+min+"kg");      
    }
}

